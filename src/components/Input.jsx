import React, { useEffect, useState } from "react";
import { useTodo } from "../Store/TodoList/Actions";
import { v1 as uuid } from "uuid";
function Input() {
  const { todos, dispatch, AddTodo } = useTodo();
  const [name, setName] = useState();


  return (
    <>
      <div className="flex items-center justify-center ">
        <div className="flex border-2 rounded">
          <input
            type="text"
            className="px-4 py-2 w-80"
            placeholder="add Project..."
            onChange={(e) => setName(e.target.value)}
            value={name}
          />
          <button
            onClick={async () => {
              await dispatch(AddTodo({ id: uuid(), name: name }));
              setName("");
            }}
            className="flex items-center justify-center px-4 border-l bg-green-400"
          >add</button>
        </div>
      </div>
    </>
  );
}

export default Input;
