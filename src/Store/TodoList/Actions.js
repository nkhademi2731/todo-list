import axios from "axios";
import { useDispatch, useSelector } from "react-redux";

export const TODO_ADD = "addTodo";
export const FETCH_DATA = "fetchData";
export const DETAIL_DATA = "detailData";
export const ADD_ISSUE = "addIssue";
export const useTodo = () => {
  const todos = useSelector((state) => state.TodoList);
  const dispatch = useDispatch();
  return {
    todos,
    dispatch,
    AddTodo,
    FetchData,
    AddIssue,
    DetailData,
  };
};
export const initAxios = () => () => {
  axios.defaults.baseURL = "https://gitlab.com/api/v4/";
  axios.defaults.headers = {
    "Content-Type": "application/x-www-form-urlencoded",
    "PRIVATE-TOKEN": "glpat-nUHDhKCWdUXRL4BJp4hC",
  };
};

const token = "glpat-nUHDhKCWdUXRL4BJp4hC";

export const FetchData = (payload) => async (dispatch) => {
  let config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const result = await axios.get("https://gitlab.com/api/v4/projects", config, {
    params: { owned: "true" },
  });

  dispatch({
    type: FETCH_DATA,
    payload: { list: result.data },
  });
};
export const DetailData = () => async (dispatch) => {
  let config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };

  const result = await axios
    .get(`https://gitlab.com/api/v4/projects/33454979/issues`, config)
   
    console.log("result", result.data);
  dispatch({
    type: DETAIL_DATA,
    payload: result.data,
  });
};

export const AddIssue = (payload) => (dispatch) => {
  console.log("AddIssue", payload);

  let config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  axios
    .post(
      `https://gitlab.com/api/v4/projects/33454979/issues`,
      {
        title: payload.taske,
      },
      config
    )
    .then((res) => {
      console.log("res", res.data);
    });
  dispatch({
    type: ADD_ISSUE,
    payload: payload,
  });
};

export const AddTodo = (payload) => (dispatch) => {
  let config = {
    headers: {
      Authorization: `Bearer ${token}`,
    },
  };
  axios
    .post(
      "https://gitlab.com/api/v4/projects",
      {
        name: payload.name,
        id: payload.id,
      },
      config
    )
    .then((res) => {
      // console.log("res", res);
    });
  dispatch({
    type: TODO_ADD,
    payload: payload,
  });
};
