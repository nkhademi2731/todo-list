import { TODO_ADD, FETCH_DATA, DETAIL_DATA } from "./Actions";
export const todoState = {
  list: null,
  detail: null,
};
export default (state = todoState, action) => {
  switch (action.type) {
    case FETCH_DATA:
      return { ...state, list: action.payload.list };
    case TODO_ADD:
      return { ...state, list: action.payload };
    case DETAIL_DATA:
      return { ...state,detail: action.payload};
  }
  return state;
};
