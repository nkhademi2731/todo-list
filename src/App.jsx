import React from "react";
import "./App.css";
import "./index.css";
import { Provider } from "react-redux";
import store from "./Store/store";
import TodList from "./View/TodList";
import Issues from "./View/Issues";
import { Route, Routes } from "react-router-dom";

export default function App() {
  const stopProp = (e) => {
    e.stopPropagation();
  };
  return (
    <>
      <div className="w-full p-8  text-center">
        <Provider store={store}>
          <Routes>
            <Route exact path="/" element={<TodList />}/>
            <Route exact path="/:id" element={<Issues />}/>
          </Routes>
        </Provider>
      </div>
    </>
  );
}
